function readBookPromise(time,book){
  console.log(`Saya mulai membaca ${book.name} `)
  return new Promise(function(resolve,reject){
    setTimeout(() => {
      let sisaWaktu = time - book.timeSpent
      if(sisaWaktu >= book.timeSpent){
        console.log(`saya telah selesai membaca ${book.name},sisa waktu saya ${sisaWaktu}`)
        resolve(sisaWaktu)
      } else {
        console.log(`saya tidak memiliki waktu untuk membaca ${book.name} 
        karena waktu saya ${time} sedangkan untuk membaca menghabiskan ${book.timeSpent}`)
        reject(time)
      }
    }, book.timeSpent);
  })
}

module.exports = readBookPromise