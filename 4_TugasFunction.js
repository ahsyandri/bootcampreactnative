// No1 -------------------------------
const teriak = () => "Halo Sanbers!"


console.log(teriak())

//No.2 -------------------------------
// - Tulislah sebuah function dengan nama kalikan() yang 
// - mengembalikan hasil perkalian dua parameter yang di kirim.

const kalikan = (bil1,bil2) => bil1*bil2

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1,num2)
console.log(hasilKali)

// No 3 ---------------------------------
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
const introduce = (name, age, address, hobby) => `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);