// 1. LOOPING WHILE --------------------------------------
var no = 2;

console.log('LOOPING PERTAMA')

while(no <= 20) { 
  console.log(no + ' - I Love Coding');
  no+=2
}

no-=2

while(no >= 2) { 
  console.log(no + ' - I will become a mobile developer');
  no-=2
}

// No. 2 Looping menggunakan for -----------------------------
// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 dan angka ganjil maka tampilkan I Love Coding.

for(let i = 1; i <= 20; i++){
  if(i%2 == 0){
    console.log(`${i} - Berkualitas`)
  }else{
    if(i%3 == 0){
      console.log(`${i} - I Love Coding`)
    }else{
      console.log(`${i} - Santai`)
    }
  }
}


// No. 3 Membuat Persegi Panjang # -----------------------------
  let panjang = 8
  let lebar   = 4
  let pagar = ''
  for(let i = 0; i < lebar ;i++){
    for(let i = 0; i < panjang ;i++){
      pagar+="#"
    } 
    pagar+='\n'
  }

console.log(pagar)



// No. 4 Membuat Tangga ----------------------------------------
  let tinggi_tangga = 5
  let tangga_awal = (tinggi_tangga - tinggi_tangga) + 1
  let pagar = ''
  
  for(let i = 0; i < 5 ;i++){
    for(let i = 0; i < tangga_awal ;i++){
      pagar+="#"
    } 
    pagar+='\n'
    tangga_awal += 1
  }

console.log(pagar)

// No. Membuat Papan Catur -------------------------------------
let pagar = ''
panjang = 6

for(let i = 0; i <panjang ;i++){
    for(let a = 0; a < 8 ;a++){
      if(i%2 == 0){
        if(a%2 != 0){
          pagar+='#'
        }else{
          pagar+=" "
        }
      }else{
        if(a%2 != 0){
          pagar+=" "
        }else{
          pagar+="#"
        }
      }
    } 
    pagar+='\n'
}
console.log(pagar)