// A. Tugas String ======================================================================================

// Soal No. 1 (Membuat kalimat), ------------------------------------------------------------------------
// Terdapat kumpulan variable dengan data string sebagai berikut
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
// Buatlah agar kata-kata di atas menjadi satu kalimat . Output:

// JavaScript is awesome and I love it! 
var complete = word + " " + second + " " + third + " " + fourth + " " + fifth + " " + sixth + " " + seventh

// Soal No.2 Mengurai kalimat (Akses karakter dalam string), ----------------------------------------------
// Terdapat satu kalimat seperti berikut:
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence.substr(5,5); 
var fourthWord = sentence.substr(11,2) 
var fifthWord = sentence.substr(14,2)
var sixthWord = sentence.substr(17,5)
var seventhWord = sentence.substr(23,6) 
var eighthWord =sentence.substr(30,9)

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String ----------------------------------------
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = exampleFirstWord3.length  
lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3); 
console.log('Third Word: ' + thirdWord3); 
console.log('Fourth Word: ' + fourthWord3); 
console.log('Fifth Word: ' + fifthWord3); 

// B. Tugas Conditional =================================================
var nama = "John"
var peran = ""

if(nama == '' && peran == ''){
  console.log("Nama harus diisi!")
}

if(nama == 'John' && peran == ''){
  console.log("Halo John, Pilih peranmu untuk memulai game!")
}

if(nama == 'Jane' && peran =='Penyihir'){
  console.log("Selamat datang di Dunia Werewolf, Jane")
  console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}

if(nama == 'Jenita' && peran =='Guard'){
  console.log("Selamat datang di Dunia Werewolf, Jenita")
  console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}

if(nama == 'Junaedi' && peran =='Werewolf'){
  console.log("Selamat datang di Dunia Werewolf, Junaedi")
  console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
} 

// Switch Case -------------------------
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

var hari = 1;

switch(true) {
  case hari < 1 && hari > 30:   { console.log('Hari Salah');  }
  case bulan < 1 && bulan > 12:   { console.log('bulan salah');  }
  case tahun < 1900 && tahun > 2200:   { console.log('Tahun Slah'); break; }
  default:  { console.log(`${hari}-${bulan}-${tahun}`); }}